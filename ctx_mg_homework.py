from contextlib import contextmanager
from datetime import datetime

@contextmanager
def date_parser(path, mode='w'):
    f = open(path, mode)

    if mode == 'w':
        date = datetime.now().strftime('%Y-%m-%d  %H:%M:%S')
        f.writelines(date + '\n')
        yield (f, date)
    else:
        date = f.readline()
        yield (f, date)

    f.close()

with date_parser('ctx_mg_file', 'r') as (f, d):
    print 'date is: %s' % d
