import json

def to_json(func):
    def wrapped(*args, **kwargs):
        return json.dumps(func(*args, **kwargs))
    return wrapped

@to_json
def func1(x):
    return x

#func1({1: 'a', 2: 'b'})

def validate_unicode(func):
    def wrapped(*args, **kwargs):
        for x in args:
            if not type(x) is unicode:
                print 'arg not unicode'
                raise TypeError

        res = func(*args, **kwargs)

        if isinstance(res, tuple):
            for el in res:
                if not type(el) is unicode:
                    print 'resp from tuple not unicode'
                    raise TypeError
        elif not type(res) is unicode:
            print 'resp not unicode'
            raise TypeError
        
        return res
    return wrapped

@validate_unicode
def tst_fct(a, b):
    ret = a + b
    #return (ret, ret.encode('utf-8'))
    return (ret, ret)
    #return ret.encode('utf-8')
    #return ret

a = u'a'
b = u'b'

resp = tst_fct(a, b)
print(resp)