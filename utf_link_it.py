# -*- coding: utf-8 -*-

import urllib2
from codecs import open

url = u'http://ko.wikipedia.org/wiki/위키백과:대문'
url_handler = urllib2.urlopen(url.encode('utf-8'))

fh = open('encoded_url_it', 'w', encoding='utf-8')

for idx, line in enumerate(url_handler):
    fh.write(str(idx) + ' ' + line.decode('utf-8'))

fh.close()