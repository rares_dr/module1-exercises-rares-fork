import codecs

def non_empty_lines(file):
    for line in file:
        if line.strip():
            yield line

def numbered_lines(lines):
    i = 0
    for line in lines:
        yield '%d: %s' % (i, line)
        i += 1

def wrap_at_column(lines, w):
    for line in lines:
        while line:
            separator = '\n' if len(line) > w else ''
            wrapped = line[:w] + separator
            line = line[w:]
            yield wrapped

def paginate(lines, page_size, page_separator='separator'):
    position = 0
    for line in lines:
        position += 1
        yield line
        if position == page_size:
            yield page_separator
            position = 0

def print_lines(lines):
    for l in lines:
        print l

# you can combine these in arbitrary ways

lines = codecs.open('encoded_url', encoding='utf-8')
lines = numbered_lines(lines)
lines = wrap_at_column(lines, 3)
lines = paginate(lines, 10)
#print_lines(lines)
