import pprint

def list_parse(lst):
    json = '['
    for x, y in enumerate(lst):
        delimiter = ',' if x > 0 else ''
        json += delimiter + generate_json(y)
    json += ']'

    return json

def dict_parse(dct):
    json = '{'
    for k, v in dct.iteritems():
        json += '"' + k + '": ' + generate_json(v) + ','
    json = json[:-1]
    json += '}'

    return json

def generate_json(struct):
    json = ''
    if isinstance(struct, list):
        json += list_parse(struct)
    elif isinstance(struct, dict):
        json += dict_parse(struct)
    else:
        json += '"' + str(struct) + '"'

    return json

struct = [ {'ana': [2, 4, 5, 6],
   'are': [{'mere': 100},
          {'pere': 23}]
  },
]

l1 = [1, {'a': 'b'}]

pprint.pprint(generate_json(struct), width=4)
