import pprint

lst = ['A/B/T', 'A/U', 'A/U/Z', 'C/D', 'C/D/F']

def unflatten(lst):
    dict1 = {}

    for path in lst:
        keys = path.split('/')
        temp_dict = dict1
        for sub_path in keys:
            if sub_path not in temp_dict:
                temp_dict[sub_path] = {}
            temp_dict = temp_dict[sub_path]

    return dict1

pprint.pprint(unflatten(lst), width=4)